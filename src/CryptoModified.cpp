/**
    New Class modified by: Rachana Nget
    Performing Encoding and Decoding
**/

#include "../include/CryptoModified.h"
#include <string>

using namespace std;

CryptoModified::CryptoModified()
{
    //ctor

}

CryptoModified::~CryptoModified()
{
    //dtor
}


string CryptoModified::encode(string s) {
    string encodedString = reverseStr(s);
    return encodedString;
}

/**
 * "decodes" by returning the input text
 *
 * @param s - the input string
 * @return the same string
 */
string CryptoModified::decode(string s) {
    string decodedString = reverseStr(s);
    return decodedString;
}




string CryptoModified::reverseStr (string s) {

    string result=""; //create a new string and set it to the empty string

        for (int i=0; i<s.length( ) ; i++) { //s.length( ) returns the length of a string
            result = s[ i ] + result ; //take the newest character and add it before what we have already
        }

return result;
}
