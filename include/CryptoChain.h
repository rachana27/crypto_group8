// Emily Blatz Homework 4

#ifndef CRYPTOCHAIN_H
#define CRYPTOCHAIN_H

#include <vector>
#include <string>
#include "Crypto.h"

class CryptoChain {
    public:
        CryptoChain();
        virtual ~CryptoChain();
        void add(Crypto *c);
        std::string encode(std::string s);
        std::string decode(std::string s);
    protected:
    private:
        std::vector<Crypto*> cryptos;
};

#endif // CRYPTOCHAIN_H
