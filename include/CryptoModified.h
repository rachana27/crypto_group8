/**
    New Class modified by: Rachana Nget
    Performing Encoding and Decoding
**/

#ifndef CRYPTOMODIFIED_H
#define CRYPTOMODIFIED_H

#include "Crypto.h"
#include <string>


class CryptoModified : public Crypto
{
    public:
        CryptoModified();
        virtual ~CryptoModified();
        // Override the pure virtual functions from the Crypto class.
        std::string encode(std::string s);
        std::string decode(std::string s);
        std::string reverseStr(std::string st);
    protected:
    private:
};

#endif // CRYPTOMODIFIED_H
