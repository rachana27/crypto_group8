
// Emily Blatz Homework 4

#ifndef CRYPTOMETHOD_H
#define CRYPTOMETHOD_H
#include <string>
#include "Crypto.h"


class CryptoMethod : public Crypto {
    public:
        CryptoMethod();
        virtual ~CryptoMethod();
        std::string encode(std::string s);
        std::string decode(std::string s);
    protected:
    private:
};

#endif // CRYPTOMETHOD_H
